<?php
$directory = __DIR__ . '/../backend/csvFiles';
$fileName = 'PreInscription.csv';
$filePath = $directory . '/' . $fileName;

if (file_exists($filePath)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="' . basename($filePath) . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filePath));
    readfile($filePath);
    exit;
} else {
    http_response_code(404);
    echo json_encode(['error' => 'File not found']);
}
?>
