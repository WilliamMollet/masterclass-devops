<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Affichage CSV</title>
    <link rel="stylesheet" href="./styles/styles.css">
</head>
<body>
    <header>
        <img src="./styles/ipssi-logo.png" alt="Logo" class="logo">
        <h1>Liste des visiteurs</h1>
    </header>
    <?php
    $directory = __DIR__ . '/../backend/csvFiles';
    $fileName = 'PreInscription.csv';
    $filePath = $directory . '/' . $fileName;

    if (!file_exists($filePath)) {
        echo "<p>Le fichier $fileName n'existe pas.</p>";
        exit();
    }

    $csvFile = fopen($filePath, 'r');

    if ($csvFile === false) {
        echo "<p>Impossible d'ouvrir le fichier $fileName.</p>";
        exit();
    }

    echo "<table>";

    // Afficher les en-têtes
    $headers = fgetcsv($csvFile);
    echo "<tr>";
    foreach ($headers as $header) {
        echo "<th>" . htmlspecialchars($header) . "</th>";
    }
    echo "<th>Détail</th>";
    echo "</tr>";

    // Afficher les données
    $id = 0;
    while (($donnees = fgetcsv($csvFile)) !== false) {
        echo "<tr>";
        foreach ($donnees as $donnee) {
            echo "<td>" . htmlspecialchars($donnee) . "</td>";
        }
        echo "<td><a href='detail.php?id=$id'><button>Détail</button></a></td>";
        echo "</tr>";
        $id++;
    }

    echo "</table>";

    fclose($csvFile);
    ?>
</body>
</html>
