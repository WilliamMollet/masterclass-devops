<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Détail du visiteur</title>
    <link rel="stylesheet" href="./styles/styles.css">
</head>
<body>
    <header>
        <img src="./styles/ipssi-logo.png" alt="Logo" class="logo">
        <h1>Détail du visiteur</h1>
    </header>
    <?php
    if (!isset($_GET['id'])) {
        echo "<p>Identifiant de visiteur non spécifié.</p>";
        exit();
    }

    $visitorId = intval($_GET['id']);
    $directory = __DIR__ . '/../backend/csvFiles';
    $fileName = 'PreInscription.csv';
    $filePath = $directory . '/' . $fileName;

    if (!file_exists($filePath)) {
        echo "<p>Le fichier $fileName n'existe pas.</p>";
        exit();
    }

    $csvFile = fopen($filePath, 'r');

    if ($csvFile === false) {
        echo "<p>Impossible d'ouvrir le fichier $fileName.</p>";
        exit();
    }

    // Lire les en-têtes
    $headers = fgetcsv($csvFile);
    $currentId = 0;
    $visitorData = null;

    // Lire les données
    while (($donnees = fgetcsv($csvFile)) !== false) {
        if ($currentId === $visitorId) {
            $visitorData = $donnees;
            break;
        }
        $currentId++;
    }

    fclose($csvFile);

    if ($visitorData === null) {
        echo "<p>Visiteur non trouvé.</p>";
        exit();
    }

    echo "<table>";
    echo "<tr><th>Champ</th><th>Valeur</th></tr>";
    foreach ($headers as $key => $header) {
        echo "<tr>";
        echo "<td>" . htmlspecialchars($header) . "</td>";
        echo "<td>" . htmlspecialchars($visitorData[$key]) . "</td>";
        echo "</tr>";
    }
    echo "</table>";
    ?>
    <p><a href="liste.php">Retour à la liste</a></p>
</body>
</html>
