<?php

use PHPUnit\Framework\TestCase;

class SaveDataTest extends TestCase
{
    public function testInputsNotEmpty()
    {
        $data = [
            'nom' => 'Doe',
            'prenom' => 'John',
            'telephone' => '1234567890',
            'email' => 'john.doe@example.com',
            'formation' => 'Formation',
            'options1' => ['Option1'],
            'options2' => ['Option2']
        ];

        $this->assertNotEmpty($data['nom'], 'Le nom ne doit pas être vide');
        $this->assertNotEmpty($data['prenom'], 'Le prénom ne doit pas être vide');
        $this->assertNotEmpty($data['telephone'], 'Le téléphone ne doit pas être vide');
        $this->assertNotEmpty($data['email'], 'L\'email ne doit pas être vide');
        $this->assertNotEmpty($data['formation'], 'La formation ne doit pas être vide');
    }

    public function testNotCharactere()
    {
        $nom = 'Doe';
        $prenom = 'John';
        $telephone = '1234567890';

        $this->assertMatchesRegularExpression('/^[a-zA-Z\s]+$/', $nom, 'Le nom ne doit pas contenir de caractères spéciaux');
        $this->assertMatchesRegularExpression('/^[a-zA-Z\s]+$/', $prenom, 'Le prénom ne doit pas contenir de caractères spéciaux');
        $this->assertMatchesRegularExpression('/^\d+$/', $telephone, 'Le téléphone ne doit contenir que des chiffres');
    }

    public function testInvalidCharacter()
    {
        $nom = 'Doe$';
        $prenom = 'J@hn';
        $telephone = '12345a7890';

        $this->assertDoesNotMatchRegularExpression('/^[a-zA-Z\s]+$/', $nom, 'Le nom contient des caractères spéciaux');
        $this->assertDoesNotMatchRegularExpression('/^[a-zA-Z\s]+$/', $prenom, 'Le prénom contient des caractères spéciaux');
        $this->assertDoesNotMatchRegularExpression('/^\d+$/', $telephone, 'Le téléphone contient des caractères non numériques');
    }

    public function testEmail()
    {
        $validEmail = 'john.doe@example.com';
        $invalidEmail1 = 'john.doeexample.com'; // Missing '@'
        $invalidEmail2 = 'john.doe@example'; // Missing top-level domain

        $this->assertMatchesRegularExpression('/^[\w\-\.]+@([\w\-]+\.)+[\w\-]{2,4}$/', $validEmail, 'L\'email est valide');
        $this->assertDoesNotMatchRegularExpression('/^[\w\-\.]+@([\w\-]+\.)+[\w\-]{2,4}$/', $invalidEmail1, 'L\'email est invalide car il manque @');
        $this->assertDoesNotMatchRegularExpression('/^[\w\-\.]+@([\w\-]+\.)+[\w\-]{2,4}$/', $invalidEmail2, 'L\'email est invalide car il manque le domaine de niveau supérieur');
    }

    public function testEmptyInput()
    {
        $data = [
            'nom' => '',
            'prenom' => '',
            'telephone' => '',
            'email' => '',
            'formation' => '',
            'options1' => [],
            'options2' => []
        ];

        $this->assertEmpty($data['nom'], 'Le nom est vide');
        $this->assertEmpty($data['prenom'], 'Le prénom est vide');
        $this->assertEmpty($data['telephone'], 'Le téléphone est vide');
        $this->assertEmpty($data['email'], 'L\'email est vide');
        $this->assertEmpty($data['formation'], 'La formation est vide');
    }
}
?>
